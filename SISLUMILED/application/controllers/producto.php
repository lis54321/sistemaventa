<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {

	public function listaProducto()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$data['producto']=$this->producto_model->retornarProducto();
		$this->load->view('admin/producto/lista',$data);
		$this->load->view('layouts/footer');
	}
	
	 
	public function agregar()
	{
		$data['categoria']=$this->producto_model->getCategoria();
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/producto/agregarForm',$data);
		$this->load->view('layouts/footer');
	}

	public function agregardb()
	{
		//$idProducto=$_POST['idProducto'];

		$categori=$_POST['categori'];
		$codigo=$_POST['codigo'];
		
		$nombreProducto=$_POST['nombreProducto'];
		$potencia=$_POST['potencia']."W";
		$flujoLuminoso=$_POST['flujoLuminoso']."Lm";
		$anguloApertura=$_POST['anguloApertura']."º";
		$temperaturaColor=$_POST['temperaturaColor']."K";
		$alimentacion=$_POST['alimentacion']."V";
		$tecionFuncionamiento=$_POST['tecionFuncionamiento']."VAC";
		$dimencionProducto=$_POST['dimencionProducto'];
		$precioCompra=$_POST['precioCompra'];
		$precioVenta=$_POST['precioVenta'];
		$stock=$_POST['stock'];
	

	  $config['upload_path'] = './assets/ImagenesBDD/productos/picture/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_size'] = '2048';  //2MB max
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $_FILES['fotopost']['name'];

	  //$this->upload->initialize($config);
	  $this->load->library('upload',$config);

	    if (!empty($_FILES['fotopost']['name'])) {
	        if ( $this->upload->do_upload('fotopost') ) {
	            $imagen = $this->upload->data();
							$data['idCategoria']=$categori;  
							$data['codigo']=$codigo;	
							$data['imagen']=$imagen['file_name'];		
							$data['nombreProducto']=$nombreProducto;		
							$data['potencia']=$potencia;		
							$data['flujoLuminoso']=$flujoLuminoso;       
							$data['anguloApertura']=$anguloApertura;       
							$data['temperaturaColor']=$temperaturaColor;
							$data['alimentacion']=$alimentacion;
							$data['tecionFuncionamiento']=$tecionFuncionamiento;       
							$data['dimencionProducto']=$dimencionProducto;        
							$data['precioCompra']=$precioCompra;        
							$data['precioVenta']=$precioVenta;
							$data['stock']=$stock;

							$this->producto_model->agregarProducto($data);
              
				redirect("producto/listaProducto",'refresh');
	        }else {
              die("gagal upload");
	        }
	    }else {
				$this->session->set_flashdata("error","Seleccione la imagen");
				redirect("producto/agregar",'refresh');
	    }
	}
	
	public function modificar()
	{
        $data=array(
			'categoria'=>$this->producto_model->getCategoria(),
			//'producto'=>$this->producto_model->getProducto(),
		);

        $idProducto=$_POST['idProducto'];
        
        $data['idCategoria']=$this->producto_model->recuperarProducto($idProducto);
		$data['codigo']=$this->producto_model->recuperarProducto($idProducto);
		$data['imagen']=$this->producto_model->recuperarProducto($idProducto);
		$data['nombreProducto']=$this->producto_model->recuperarProducto($idProducto);
		$data['potencia']=$this->producto_model->recuperarProducto($idProducto);
		$data['flujoLuminoso']=$this->producto_model->recuperarProducto($idProducto);
		$data['anguloApertura']=$this->producto_model->recuperarProducto($idProducto);
		$data['temperaturaColor']=$this->producto_model->recuperarProducto($idProducto);
		$data['alimentacion']=$this->producto_model->recuperarProducto($idProducto);
		$data['tecionFuncionamiento']=$this->producto_model->recuperarProducto($idProducto);
        $data['dimencionProducto']=$this->producto_model->recuperarProducto($idProducto);
        $data['precioCompra']=$this->producto_model->recuperarProducto($idProducto);
        $data['precioVenta']=$this->producto_model->recuperarProducto($idProducto);
        $data['stock']=$this->producto_model->recuperarProducto($idProducto);


		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/producto/modificarForm',$data);
		$this->load->view('layouts/footer');
	}




	public function modificardb()
	{
        $idProducto=$_POST['idProducto'];
        
		$categori=$_POST['categori'];
		$codigo=$_POST['codigo'];		
		$nombreProducto=$_POST['nombreProducto'];
		$potencia=$_POST['potencia'];
		$flujoLuminoso=$_POST['flujoLuminoso'];
		$anguloApertura=$_POST['anguloApertura'];
		$temperaturaColor=$_POST['temperaturaColor'];
		$alimentacion=$_POST['alimentacion'];
		$tecionFuncionamiento=$_POST['tecionFuncionamiento'];
		$dimencionProducto=$_POST['dimencionProducto'];
		$precioCompra=$_POST['precioCompra'];
		$precioVenta=$_POST['precioVenta'];
		$stock=$_POST['stock'];

		$path = './assets/ImagenesBDD/productos/picture/';


		// get foto
		$config['upload_path'] = './assets/ImagenesBDD/productos/picture/';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['max_size'] = '2048';  //2MB max
		$config['max_width'] = '4480'; // pixel
		$config['max_height'] = '4480'; // pixel
		$config['file_name'] = $_FILES['fotopost']['name'];

		//$this->upload->initialize($config);		
		$this->load->library('upload',$config);

			if (!empty($_FILES['fotopost']['name'])) {
				if ( $this->upload->do_upload('fotopost') ) {
					$imagen = $this->upload->data();
					$data['idCategoria']=$categori;  
							$data['codigo']=$codigo;	
							$data['imagen']=$imagen['file_name'];		
							$data['nombreProducto']=$nombreProducto;		
							$data['potencia']=$potencia;		
							$data['flujoLuminoso']=$flujoLuminoso;       
							$data['anguloApertura']=$anguloApertura;       
							$data['temperaturaColor']=$temperaturaColor;
							$data['alimentacion']=$alimentacion;
							$data['tecionFuncionamiento']=$tecionFuncionamiento;       
							$data['dimencionProducto']=$dimencionProducto;        
							$data['precioCompra']=$precioCompra;        
							$data['precioVenta']=$precioVenta;
							$data['stock']=$stock;

				// hapus foto pada direktori
				@unlink($path.$_POST['filelama']);
					$this->producto_model->modificarProducto($idProducto,$data);
					redirect('producto/listaProducto','refresh');
				}else {
				die("gagal update");
				}
			}else {
			echo "tidak masuk";
		}

	}

	public function eliminardb()
	{
		$idProducto=$_POST['idProducto'];

		//$categori=$_POST['categori'];
        //$data['idCategoria']=$categori;
        
		$codigo=$_POST['codigo'];
		$data['codigo']=$codigo;
		
		//$imagen=$_POST['imagen'];
		//$data['imagen']=$imagen;
		
		$nombreProducto=$_POST['nombreProducto'];
		$data['nombreProducto']=$nombreProducto;
		
		$potencia=$_POST['potencia'];
		$data['potencia']=$potencia;
		
		
        $precioVenta=$_POST['precioVenta'];
        $data['precioVenta']=$precioVenta;
        
        $stock=$_POST['stock'];
        $data['stock']=$stock;

        $estado=0;
        $data['estado']=$estado;
        
		$this->producto_model->eliminarProducto($idProducto,$data);
		redirect('producto/listaProducto','refresh');

    }
}
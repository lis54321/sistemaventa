<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogo_model extends CI_Model {

	public function retornarCatalogo()
	{
        $this->db->select('idProducto,imagen,nombreProducto,precioVenta');
		$this->db->from('producto');
		$this->db->where('estado',1);
		return $this->db->get();
	}



	public function retornarProducto()
	{
		$this->db->select('*');
		$this->db->from('producto');
		$this->db->where('estado',1);
		return $this->db->get();
	}

	public function jumlah_row($search)
	{
	  $this->db->or_like($search);
	  $query = $this->db->get('producto');
  
	  return $query->num_rows();
	}
	
	public function get($batas=NULL,$offset=NULL,$cari=NULL)
	{
		if ($batas != NULL) {
		  $this->db->limit($batas,$offset);
		}
		if ($cari != NULL) {
			$this->db->or_like($cari);
		}
		$this->db->from('producto');
		$query = $this->db->get();
		return $query->result();
	}


}
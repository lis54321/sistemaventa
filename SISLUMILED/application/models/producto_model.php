<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto_model extends CI_Model {

	public function retornarProducto()
	{
		$this->db->select('P.idProducto,C.nombreCategoria as categori,P.codigo,P.imagen,P.nombreProducto,P.potencia,P.precioVenta,P.stock');
		$this->db->from('producto P');
		$this->db->join('categoria C','P.idCategoria = C.idCategoria');
		$this->db->where('P.estado',1);
		return $this->db->get();
	}
	public function getCategoria()//para extraeer los datos de la otra tabla join
	{
		$resultados=$this->db-> get('categoria');
		return $resultados->result();
	}

	public function agregarProducto($data)
	{
		$this->db->insert('producto',$data);

	}
	
	public function recuperarProducto($idProducto)
	{
		$this->db->select('*');
		$this->db->from('producto');
		$this->db->where('idProducto',$idProducto);
		return $this->db->get();
	}
	
	public function modificarProducto($idProducto,$data)
	{
		$this->db->where('idProducto',$idProducto);
		$this->db->update('producto',$data);
	}
	
	public function eliminarProducto($idProducto,$data)
	{
		$this->db->where('idProducto',$idProducto);
		$this->db->update('producto',$data);
	}


	/// para detalle venta solo de un producto datos 
 	
	public function getProducto($idProducto)
	{
		$this->db->where('idProducto',$idProducto);
		$resul=$this->db->get('producto');
		return $resul->row();
	}


}
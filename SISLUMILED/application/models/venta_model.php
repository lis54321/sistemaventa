<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta_model extends CI_Model {
  ////datos de la vista de la lista de ventas 

  public function retornarVenta()
  {
	  $this->db->select('V.idVenta,C.razonSocial,C.nit,V.total,V.fechaRegistro');
	  $this->db->from('venta V ');
	  $this->db->join('cliente C','V.idCliente = C.idCliente');
	  $this->db->where('V.estado',1);
	  return $this->db->get();
	  
  }

  public function retornarDetalleVenta($id)
  {
	  $this->db->select('DV.*,P.imagen,P.codigo,P.nombreProducto');
	  $this->db->from('detalleventa DV ');
	  $this->db->join('producto P','DV.idProducto = P.idProducto');
	  $this->db->where('DV.idVenta',$id);
	  return $this->db->get();
	 
	  //$resultados=$this->db->get();
	  //return $resultados-result();
  }
  public function retornarRecibo($id)
  {
	  $this->db->select('V.idVenta,C.razonSocial,C.nit,V.total,V.fechaRegistro');
	  $this->db->from('venta V ');
	  $this->db->join('cliente C','V.idCliente = C.idCliente');
	  $this->db->where('V.idVenta',$id);
	  $resultados=$this->db->get();
	  return $resultados->row();
	  //return $this->db->get();
  }






  public function agregaVenta($data)
  {
	  $this->db->trans_start();
	  $this->db->insert('venta',$data);

	  $idVenta=$this->db->insert_id();

	  $idProducto=$_POST['idProducto'];
	  $precio=$_POST['precio'];
	  $cantidad=$_POST['cantidad'];
	  $importe=$_POST['importe'];


	  $data['idVenta']=$idVenta;
	  $data['idProducto']=$producto[$i];
	  $data['precio']=$precio[$i];
	  $data['cantidad']=$cantidad[$i];
	  $data['importe']=$importe[$i];


	  $this->db->where('idVenta',$idVenta);
	  $this->db->insert('detalleVenta',$data);
  
	  if ($this->db->trans_status() === FALSE)
	  {
			  $this->db->trans_rollback();
	  }
	  else
	  {
			  $this->db->trans_commit();
	  }
	  return $this->db->trans_status();
  }




  
    public function agregarVenta($data)
	{
		return $this->db->insert('venta',$data);
    }
     //ultimo id de la tabla ventas
    public function ultimoID()
	{
		return $this->db->insert_id();
    }

    public function guardarDetallee($data)
	{
	    $this->db->insert('detalleventa',$data);
	}

} 
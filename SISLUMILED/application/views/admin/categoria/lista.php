        <div class="content-wrapper">
            
            <section class="content-header">
                <br>
                <h1 class="text-center">
                CATEGORIA DE PRODUCTOS
                </h1>
            </section>
            <section class="content-header">
                <?php echo form_open_multipart('categoria/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>

                <a href="<?=base_url()?>admin/listaUsuarioPdf" class="btn btn-oval btn-danger" type="submit">Exportar a PDF <i class="fa fa-file-pdf-o "></i></a>
                <?php echo form_close(); ?>
            </section>
            <!-- Main content -->

            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        
                        <table class="table table-striped table-inverse table-responsive" id="tabel">
                            
                            <thead class="thead-inverse">
                                <tr>
                                <th>#</th>
                                <th>NOMBRE</th>
                                <th>DESCRIPCION</th>
                                <th style="width: 130px">OPCIONES</th>
                                
                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($categoria->result() as $row) {
                                ?>
                                    <tr>
                                        <td><?php echo $indice; ?></td>
                                        <td><?php echo $row->nombreCategoria; ?></td>
                                        <td><?php echo $row->descripcion; ?></td>
                                        <?php $dataCategoria = $indice."*".$row->nombreCategoria."*".$row->descripcion;?>
                    
                                        <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('categoria/modificar'); ?>
                                                <input type="hidden" name="idCategoria" value="<?php echo $row->idCategoria; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('categoria/eliminardb'); ?>
                                                    <input type="hidden" name="idCategoria" value="<?php echo $row->idCategoria; ?>">
                                                    <input type="hidden" name="nombreCategoria" value="<?php echo $row->nombreCategoria; ?>">
                                                    <input type="hidden" name="descripcion" value="<?php echo $row->descripcion; ?>">
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>      
            </section>
        </div>
       


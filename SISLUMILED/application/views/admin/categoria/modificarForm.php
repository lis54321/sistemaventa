
        <div class="content-wrapper">
            <section class="content-header">
                <h1 class="text-center">
                  MODIFICANDO
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                foreach ($nombreCategoria->result() as $row) {
                                ?>
                                <?php echo form_open_multipart('categoria/modificardb'); ?>
                            
                                <form action="/action_page.php">
                                    <div class=form-group>
                                    <input type="hidden" name="idCategoria" value="<?php echo $row->idCategoria; ?>">
                                     </div >
                                    <div class=form-group>
                                        <label for="nombreCategoria">Categoria: </label>
                                        <input type="text" name="nombreCategoria" class="form-control" id="nombreCategoria" value="<?php echo $row->nombreCategoria; ?>"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >        
                                    <div class=form-group>
                                        <label for="descripcion">Descripción: </label>
                                        <input type="text" name="descripcion" placeholder="Descripción del producto.."class="form-control" id="descripcion" value="<?php echo $row->descripcion; ?>" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >         
                                    <hr>                   
                                    <div class=form-group>
                                    
                                        <button type="submit" class="btn btn-oval btn-primary pull-center">Registrar</button>
                                        <?php echo form_close(); ?>
                                    
                                    <a href="<?=base_url()?>index.php/categoria/listaCategoria" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div >  
                                          
                                    <?php
                                    }
                                    ?>
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       

        <div class="content-wrapper">
            <section class="content-header">
                <a href="<?php echo base_url();?>index.php/categoria/listaCategoria">Ir atras</a>
            </section>
            <section class="content-header">
                <h1>
                REGISTRO DE UN NUEVO CATEGORIA
                </h1>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">

                                <?php if($this->session->flashdata("error")):?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                                </div>
                                <?php endif;?>

                                 <?php echo form_open_multipart('categoria/agregardb'); ?>
                                
                                 <div>
                                      
                                    <form method="POST">
                                    
                                        <div class="form-group">
                                            <label for="nombreCategoria">Categoria: </label>
                                            <input type="text" name="nombreCategoria" placeholder="Ingrese el nombre de la categoria.."class="form-control" id="nombreCategoria" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >
                                        <div class=form-group>
                                            <label for="descripcion">Descripción: </label>
                                            <input type="text" name="descripcion" placeholder="Descripción de la categoria .."class="form-control" id="descripcion"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico." >
                                        </div >                
                                        <div>
                    
                                            <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                            <?php echo form_close(); ?>

                                            <a href="<?=base_url()?>index.php/categoria/listaCategoria" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                        </div >  
                                        
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       

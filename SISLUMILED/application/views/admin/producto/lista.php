
        <div class="content-wrapper">
            
            <section class="content-header">
                <br>
                <h1 class="text-center">
                PRODUCTOS
                </h1>
            </section>
            <section class="content-header">
                <?php echo form_open_multipart('producto/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>

                <a href="<?=base_url()?>admin/listaUsuarioPdf" class="btn btn-oval btn-danger" type="submit">Exportar a PDF <i class="fa fa-file-pdf-o "></i></a>
                <?php echo form_close(); ?>
            </section>
            <!-- Main content -->

            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped table-inverse " id="tabel">
                            
                            <thead class="thead-inverse">
                                    <th style="width: 3%">Nº</th>
                                    <th style="width: 5%">IMAGEN</th>
                                    <th>CATEGORIA</th>
                                    <th>CODIGO</th>                                    
                                    <th>NOMBRE</th>                       
                                    <th>PRECIO/Bs</th>
                                    <th>STOCK</th>
                                <th style="width: 80px">OPCIONES</th>
                                
                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($producto->result() as $row) {
                                ?>
                                    <tr>
                                        <td><?php echo $indice; ?></td>
                                        <td><a href="<?=base_url().'assets/ImagenesBDD/productos/picture/'.$row->imagen;?>" target="_blank"><img src="<?=base_url().'assets/ImagenesBDD/productos/picture/'.$row->imagen;?>" width="50" ></a></td>
                                        <td><?php echo $row->categori; ?></td>
                                        <td><?php echo $row->codigo; ?></td>
                                        <td><?php echo $row->nombreProducto; ?></td>                               
                                        <td><?php echo $row->precioVenta; ?></td>
                                        <td><?php echo $row->stock; ?></td>
                                        

                                        <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('producto/modificar'); ?>
                                                <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('producto/eliminardb'); ?>
                                                    <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">

                                                    <input type="hidden" name="codigo" value="<?php echo $row->codigo; ?>">
                                                    <input type="hidden" name="imagen" value="<?php echo $row->imagen; ?>">
                                                    <input type="hidden" name="nombreProducto" value="<?php echo $row->nombreProducto; ?>">
                                                    <input type="hidden" name="potencia" value="<?php echo $row->potencia; ?>">
                                                    <input type="hidden" name="precioVenta" value="<?php echo $row->precioVenta; ?>">
                                                    <input type="hidden" name="stock" value="<?php echo $row->stock; ?>">
                                

                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>      
            </section>
        </div>
       
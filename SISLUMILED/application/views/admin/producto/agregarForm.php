        <div class="content-wrapper">
            <section class="content-header">
                <a href="<?php echo base_url();?>index.php/producto/listaProducto">Ir atras</a>
            </section>
            <section class="content-header">
                <h1>
                REGISTRO DE PRODUCTOS
                <small>Nuevo</small>
                </h1>
            </section>

           
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">

                                <?php if($this->session->flashdata("error")):?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata("error");?></p>
                                </div>
                                <?php endif;?>

                                <?php echo form_open_multipart('producto/agregardb'); ?>
                                <div>

                                <form>
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="categori">Categoria: </label>
                                        <select name="categori" class="form-control" id="categori" >
                                            <?php foreach($categoria as $categori):?>
                                                <option value="<?php echo $categori->idCategoria;?>"><?php echo $categori->nombreCategoria; ?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div >
                                    </div >

                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="codigo">Codigo: </label>
                                        <input type="text" name="codigo" placeholder="Ingrese el codigo.."class="form-control" id="codigo" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener letras y números.">
                                    </div >
                                    </div >

                                    <div class="col-md-1">
                                    <div class=form-group>
                                        <label for="imagen">Imagen: </label>
                                        
                                        <input type="file" name="fotopost">
                                    </div >  
                                    </div >

                                    <br>
                                    <div class="col-md-12">
                                    <div class=form-group>
                                        <label for="nombreProducto">Nombre: </label>
                                        <input type="text" name="nombreProducto" placeholder="Ingrese el nombre de producto.."class="form-control" id="nombreProducto" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfaveticos." >
                                    </div >
                                    </div > 

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="potencia">Potencia: </label>
                                        <input type="text" name="potencia" placeholder="Ingrese la potencia.."class="form-control" id="potencia" pattern="[0-9\s]+" required="" title="Solo puede contener números." >
                                    </div >
                                    </div > 

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="flujoLuminoso">Flujo Luminoso: </label>
                                        <input type="text" name="flujoLuminoso" placeholder="Ingrese el flujo luminoso.."class="form-control" id="flujoLuminoso" pattern="[0-9\s]+" required="" title="Solo puede contener números.">
                                    </div > 
                                    </div >

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="anguloApertura">Ángulo de Apertura: </label>
                                        <input type="text" name="anguloApertura" placeholder="Ingrese el águlo de apertura.."class="form-control" id="anguloApertura" pattern="[0-9\s]+" required="" title="Solo puede contener números." >
                                    </div > 
                                    </div >

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="temperaturaColor">Temperatura de Color: </label>
                                        <input type="text" name="temperaturaColor" placeholder="Ingrese la temperatura de calor.."class="form-control" id="temperaturaColor" pattern="[0-9/\s]+" required="" title="Solo puede contener números." >
                                    </div > 
                                    </div >

                                    <br>
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="alimentacion">Alimentación: </label>
                                        <input type="text" name="alimentacion" placeholder="Ingrese la alimentación.."class="form-control" id="alimentacion" pattern="[0-9\s]+" required="" title="Solo puede contener caracteres númericos." >
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="tecionFuncionamiento">Tención de Funcionamiento: </label>
                                        <input type="text" name="tecionFuncionamiento" placeholder="Ingrese la tención de funcionamiento.."class="form-control" id="tecionFuncionamiento" pattern="[0-9=\s]+" required="" title="Solo puede contener caracteres númericos" >
                                    </div > 
                                    </div >                                

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="dimencionProducto">Dimencion: </label>
                                        <input type="text" name="dimencionProducto" placeholder="Describa la dimencion de producto.."class="form-control" id="dimencionProducto" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener letras y números." >
                                    </div > 
                                    </div >
                                    <br>

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label class="col-md-12" for="precioCompra">Precio de Compra: </label>
                                        <div class="col-md-11">
                                        <input type="text" name="precioCompra" placeholder="Ingrese el precio.."class="form-control" id="precioCompra" pattern="[0-9.\s]+" required="" title="Solo puede contener números y 00.00 bs." >
                                        </div >
                                        <label for="stock">-Bs.</label>
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label class="col-md-12" for="precioVenta">Precio de Venta: </label>
                                        <div class="col-md-11">
                                        <input type="text" name="precioVenta" placeholder="Ingrese el precio.."class="form-control" id="precioVenta" pattern="[0-9.\s]+" required="" title="Solo puede contener números y 00.00 bs." >
                                        </div >
                                        <label  for="stock">-Bs.</label>
                                        
                                    </div >
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label class="col-md-12" for="stock">Stock: </label>
                                        <div class="col-md-10">
                                        <input type="text" name="stock" placeholder="Ingrese el stock.."class="form-control" id="stock" pattern="[0-9.\s]+" required="" title="Solo puede contener números y 00.00 bs." >
                                        </div >
                                        <label for="und">-Und.</label>
                                    </div >
                                    </div >
                                    
                                    
                                    <div class="col-md-12">
                                    <br>
                                    <br>
                                    <div>
                
                                        <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                        <?php echo form_close(); ?>

                                        <a href="<?=base_url()?>index.php/producto/listaProducto" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div > 
                                    </div > 
                                    
                                </form>
                                </div > 
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       

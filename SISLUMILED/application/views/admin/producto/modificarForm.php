
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                  MODIFICANDO
                <small>Listas</small>
                </h1>
            </section>
            <a href="<?php echo base_url();?>index.php/producto/listaProducto">Ir atras</a>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                foreach ($codigo->result() as $row) {
                                ?>
                                <?php echo form_open_multipart('producto/modificardb'); ?>
                            
                                <form>
                                    <div class=form-group>
                                        <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">
                                     </div >

                                     <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="categori">Categoria: </label>
                                        <select name="categori" class="form-control" id="categori" >
                                           
                                            
                                            <?php foreach($categoria as $categori):?>
                                            <?php if($categori->idCategoria == $producto->idCategoria):?>
                                            
                                            <option value="<?php echo $categori->idCategoria;?>" selected ><?php echo $categori->nombreCategoria;?></option>
                                            <?php else:?>
                                            <option value="<?php echo $categori->idCategoria;?>" ><?php echo $categori->nombreCategoria;?></option>
                                            <?php endif; ?>
                                            <?php endforeach;?>




                                        </select> 
                                    </div >
                                    </div >

                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="codigo">Codigo: </label>
                                        <input type="text" name="codigo" placeholder="Ingrese el codigo.."class="form-control" id="codigo" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener letras y números."value="<?php echo $row->codigo; ?>">
                                    </div >
                                    </div >

                                    <div class="col-md-1">
                                    <div class=form-group>
                                        <!--<label for="imagen">Imagen: </label>-->
                                        
                                        <a href="#" class="thumbnail">
                                             <img src="<?=base_url()?>assets/ImagenesBDD/productos/picture/<?=$row->imagen?>" alt="imagen">
                                        </a>
                                        <input type="file" name="fotopost">                            
                                        <input type="hidden" name="filelama" value="<?=$row->imagen?>">
                                    </div >  
                                    </div >

                                    <br>
                                    <div class="col-md-12">
                                    <div class=form-group>
                                        <label for="nombreProducto">Nombre: </label>
                                        <input type="text" name="nombreProducto" placeholder="Ingrese el nombre de producto.."class="form-control" id="nombreProducto" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfaveticos." value="<?php echo $row->nombreProducto; ?>">
                                    </div >
                                    </div > 

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="potencia">Potencia: </label>
                                        <input type="text" name="potencia" placeholder="Ingrese la potencia.."class="form-control" id="potencia" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener caracteres alfa númericos." value="<?php echo $row->potencia; ?>">
                                    </div >
                                    </div > 

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="flujoLuminoso">Flujo Luminoso: </label>
                                        <input type="text" name="flujoLuminoso" placeholder="Ingrese el flujo luminoso.."class="form-control" id="flujoLuminoso" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener números."value="<?php echo $row->flujoLuminoso; ?>">
                                    </div > 
                                    </div >

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="anguloApertura">Ángulo de Apertura: </label>
                                        <input type="text" name="anguloApertura" placeholder="Ingrese el águlo de apertura.."class="form-control" id="anguloApertura" pattern="[A-Za-z0-9º\s]+" required="" title="Solo puede contener números." value="<?php echo $row->anguloApertura; ?>">
                                    </div > 
                                    </div >

                                    <div class="col-md-3">
                                    <div class=form-group>
                                        <label for="temperaturaColor">Temperatura de Color: </label>
                                        <input type="text" name="temperaturaColor" placeholder="Ingrese la temperatura de calor.."class="form-control" id="temperaturaColor" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener números." value="<?php echo $row->temperaturaColor; ?>">
                                    </div > 
                                    </div >

                                    <br>
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="alimentacion">Alimentación: </label>
                                        <input type="text" name="alimentacion" placeholder="Ingrese la alimentación.."class="form-control" id="alimentacion" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener caracteres númericos." value="<?php echo $row->alimentacion; ?>">
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="tecionFuncionamiento">Tención de Funcionamiento: </label>
                                        <input type="text" name="tecionFuncionamiento" placeholder="Ingrese la tención de funcionamiento.."class="form-control" id="tecionFuncionamiento" pattern="[A-Za-z0-9-\s]+" required="" title="Solo puede contener caracteres númericos" value="<?php echo $row->tecionFuncionamiento; ?>">
                                    </div > 
                                    </div >                                

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="dimencionProducto">Dimencion: </label>
                                        <input type="text" name="dimencionProducto" placeholder="Describa la dimencion de producto.."class="form-control" id="dimencionProducto" pattern="[A-Za-z0-9\s]+" required="" title="Solo puede contener letras y números." value="<?php echo $row->dimencionProducto; ?>">
                                    </div > 
                                    </div >
                                    <br>

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label class="col-md-12" for="precioCompra">Precio de Compra: </label>
                                        <div class="col-md-11">
                                        <input type="text" name="precioCompra" placeholder="Ingrese el precio.."class="form-control" id="precioCompra" pattern="[0-9.\s]+" required="" title="Solo puede contener números y 00.00 bs." value="<?php echo $row->precioCompra; ?>">
                                        </div >
                                        <label for="stock">-Bs.</label>
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label class="col-md-12" for="precioVenta">Precio de Venta: </label>
                                        <div class="col-md-11">
                                        <input type="text" name="precioVenta" placeholder="Ingrese el precio.."class="form-control" id="precioVenta" pattern="[0-9.\s]+" required="" title="Solo puede contener números y 00.00 bs." value="<?php echo $row->precioVenta; ?>">
                                        </div >
                                        <label  for="stock">-Bs.</label>
                                        
                                    </div >
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label class="col-md-12" for="stock">Stock: </label>
                                        <div class="col-md-10">
                                        <input type="text" name="stock" placeholder="Ingrese el stock.."class="form-control" id="stock" pattern="[0-9.\s]+" required="" title="Solo puede contener números y 00.00 bs." value="<?php echo $row->stock; ?>">
                                        </div >
                                        <label for="und">-Und.</label>
                                    </div >
                                    </div >
                                    
                                    
                                    <div class="col-md-12">
                                    <br>
                                    <br>
                                    <div>
                
                                        <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                        <?php echo form_close(); ?>

                                        <a href="<?=base_url()?>index.php/producto/listaProducto" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div > 
                                    </div >

                                    <?php echo form_close(); ?>
                                    <?php
                                    }
                                    ?>
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       

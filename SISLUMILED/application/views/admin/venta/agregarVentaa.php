<section id="services">
      <div class="container">

        <div class="content-wrapper">
            
            <section class="content-header">
                <br>
                <h1 class="text-center">
                VENTA DE PRODUCTOS
                </h1>
            </section>


            <!-- Main content -->

            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <?php echo form_open_multipart('venta/agregardb'); ?>
                            <div class="form-group">
                                <div class="col-md-12">
                                   
                                    <div class="col-md-1">
                                        <label for="potencia">Buscar</label>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-defaul" ><span class="fa fa-search"></span>....</button>
                                        </span>
                                    </div >
                                    <div class="col-md-4">
                                        <div class=form-group>
                                            <label for="potencia">Cliente </label>
                                            <input type="hidden" name="idCliente" id="idCliente">
                                            <input type="text" class="form-control" disabled="disabled" id="cliente"></div >
                                        
                                    </div >
                                    <div class="col-md-4">
                                        <div class=form-group>
                                            <label for="potencia">NIT </label>
                                            <input type="text" class="form-control" disabled="disabled" id="nit">
                                        
                                    </div >
                                </div>
                            </div>
                          <!--<div class="col-md-12">
                              <span class="input-group-btn">
                                  <a href="<?=base_url()?>index.php/catalogoVenta/listaProducto"  class="btn btn-oval btn-success" type="submit">Agregar Productos <i class="fa fa-plus "></i></a>
                               </span>
                          </div >-->
                          <hr>
                          <hr>
                          <hr>

                         
                          <br>
                          <br>
                            <div class="col-md-1">
                            <br><br>
                                <label for="potencia">Buscar</label>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-defaull" ><span class="fa fa-search"></span>....</button>
                                </span>
                            </div >
                            <hr>
                            <br>
                            <br>
                        
                            <div class="table-responsive">
                                <table id="tbventas" class="table table-striped table-inverse ">   
                                    <thead class="thead-inverse">

                                            <th style="width: 5%">CODIGO</th>
                                            <th>PRODUCTO</th>                                            
                                            <th>STOCK MAX</th>
                                            <th>PRECIO/Bs</th>
                                            <th>CANTIDAD</th>
                                            <th>IMPORTE</th>
                                        
                                        <th style="width: 130px"></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                       



                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div class="form-group">
                               
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">Total:</span>
                                        <input type="text" class="form-control" name="total" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="text-center">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-oval btn-primary">CONFIRMAR VENTA</button>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                               
                        </div>
                    </div>  
                </div>      
            </section>
        </div>





<div class="modal fade" id="modal-defaul">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CLIENTES</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                <table class="table table-striped table-inverse " id="tabel">
                          
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>RAZON SOCIAL</th>
                            <th>NIT-CI</th>
                            <th>OPCIÓN</th>
                        </tr>
                    </thead>
                     <tbody>
                        <?php
                        $indice=1;
                        foreach ($cliente->result() as $row) {
                        ?>
                    <tr>
                    <td> <?php echo $indice;?></td>
                    <td><?php echo $row->razonSocial;?></td>
                    <td><?php echo $row->nit;?></td>

                    <?php $datacliente = $row->idCliente."*".$row->razonSocial."*".$row->nit;?>

                    <td>
                        <button type="button" class="btn btn-danger btn-check" value="<?php echo $datacliente;?>">
                        <i class="fa fa-check-square-o"></i>
                        </button>
                     </td>
                    </tr>
        
                      <?php
                      $indice++;
                      }
                      ?>   
                    </tbody>
                </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        
    </div>
 
</div>







<div class="modal fade" id="modal-defaull">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">PRODUCTOS</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                <table class="table table-striped table-inverse " id="example1">
                          
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th>IMEGEN</th>
                            <th>CODIGO</th>
                            <th>PRODUCTO</th>      
                            <th>PRECIO/Bs</th>
                            <th>AGREGAR</th>
                        </tr>
                    </thead>
                     <tbody>
                        <?php
                        $indice=1;
                        foreach ($producto->result() as $row) {
                        ?>
                    <tr>
                        <td><?php echo $indice; ?></td>
                        <td><a href="<?=base_url().'assets/ImagenesBDD/productos/picture/'.$row->imagen;?>" target="_blank"><img src="<?=base_url().'assets/ImagenesBDD/productos/picture/'.$row->imagen;?>" width="50" ></a></td>
                        <td><?php echo $row->codigo; ?></td>
                        <td><?php echo $row->nombreProducto; ?></td>                               
                        <td><?php echo $row->precioVenta; ?></td>

                    <?php $dataProducto = $row->idProducto."*".$row->codigo."*".$row->nombreProducto."*".$row->stock."*".$row->precioVenta;?>

                    <td>
                        <div class="col-md-2">
                        <button type="button" class="btn btn-danger btn-checkk" value="<?php echo $dataProducto;?>">
                        <i class="fa fa-check-square-o"></i>
                        </button>
                         </div>
                     </td>
                    </tr>
        
                      <?php
                      $indice++;
                      }
                      ?>   
                    </tbody>
                </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        
    </div>
 
</div>






</div>      
</section>


<section id="services">
      <div class="container">

        <div class="content-wrapper">
            <section class="content-header">
                <h1 class="text-center">
                  MODIFICANDO CANTIDAD DE PRODUCTO
                </h1>
            </section>
            <a href="<?php echo base_url();?>index.php/venta/listaCarroProducto">Ir atras</a>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">


                <?php
                foreach ($carroproducto->result() as $row) {
                ?>
                <?php echo form_open_multipart('venta/modificardb'); ?>
            
                <form action="/action_page.php">
                    <div class=form-group>
                    <input type="hidden" name="idCarroProducto" value="<?php echo $row->idCarroProducto; ?>">
                     </div >
                    <div class=form-group>
                        <label for="codigo">Codigo: </label>
                        <input  type="text" name="codigo"class="form-control" disabled="disabled" id="codigo" value="<?php echo $row->codigo; ?>">
                    </div >  
                    <div class=form-group>
                        <label for="nombreProducto">Nombre: </label>
                        <input  type="text" name="nombreProducto"class="form-control" disabled="disabled" id="nombreProducto" value="<?php echo $row->nombreProducto; ?>">
                    </div >   
                    <div class=form-group>
                        <label for="precioVenta">Precio: </label>
                        <input  type="text"name="precioVenta"class="form-control"disabled="disabled" id="precioVenta" value="<?php echo $row->precioVenta; ?>">
                    </div >   
                    <div class=form-group>
                        <label for="cantidad">Cantidad: </label>
                        <input  type="text" name="cantidad" class="form-control" id="cantidad" pattern="[0-9]+" required="" title="Solo puede contener números." value="<?php echo $row->cantidad; ?>">                     
                        </div >  
                    <hr>                   
                    <div class=form-group>
                    

                        <button type="submit" class="btn btn-oval btn-primary pull-center">Guargar</button>
                        <?php echo form_close(); ?>
                    
                   </div >  
                          
                    <?php
                    }
                    ?>
                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
    </div>
                <!-- /.box -->
</section>
       
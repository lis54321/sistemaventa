<section id="services">
    <div class="container">
       
       
       
       <div class="content-wrapper">
            
            <section class="content-header">
                <br>
                <h1 class="text-center">
                LISTA DE VENTAS
                </h1>
            </section>
            <section class="content-header">
            <?php echo form_open_multipart('venta/listaDetVenta/#services'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>

                <a href="<?=base_url()?>admin/listaUsuarioPdf" class="btn btn-oval btn-danger" type="submit">Exportar a PDF <i class="fa fa-file-pdf-o "></i></a>
                <?php echo form_close(); ?>
            </section>
            <!-- Main content -->

            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>

                        <div class="table-responsive">
                        <table class="table table-striped table-inverse " id="tabel">
                    
                            <thead class="thead-inverse">
                                <tr>
                                <th>Nª</th>
                                <th>NOMBRE DEL CLIENTE</th>
                                <th>TOTAL IMPORTE/BS</th>
                                <th>FECHA DE VENTA</th>
                                <th style="width: 130px">OPCIONES</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                               <?php
                                $indice=1;
                                foreach ($venta->result() as $row) {
                                ?>
                                    <tr>
                                        <td><?php echo $indice; ?></td>
                                        <td><?php echo $row->razonSocial; ?></td>
                                        <td><?php echo $row->total; ?></td>
                                        <td><?php echo $row->fechaRegistro; ?></td>
                                        <td>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('venta/view'); ?>
                                                 <!--<input type="hidden" name="idVenta" value="<//?php echo $row->idVenta; ?>"> no afecta--> 
                                                 <button type="button" class="btn btn-info btn-view-venta" data-target="#modal-default" data-toggle="modal" value="<?php echo $row->idVenta; ?>">
                                                 <span class="fa fa-search"></span>
                                                 </button>
                                             <?php echo form_close(); ?>   
                                        </div>
                                        <div class="btn-group">  
                                            <?php echo form_open_multipart('venta/recibo'); ?>
                                                <!--<input type="hidden" name="idVenta" value="<//?php echo $row->idVenta; ?>"> no afecta--> 
                                                <button type="button" class="btn btn-danger btn-view-venta" data-target="#modal-defaultt" data-toggle="modal" value="<?php echo $row->idVenta; ?>">
                                                <span class="fa fa-print">Recibo</span>
                                                </button>
                                            <?php echo form_close(); ?> 
                                        </div>
                                        </td>
                                    </tr> 
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>      
            </section>
        </div>
        
    </div>      
</section>

 <!-- /.modal para mostrar los productos vendidos -->
        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Productos Vendidos </h4>
              </div>
              <div class="modal-body">
               



              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info btn-print"> <span class="fa fa-print">Imprimir</span></button>
              </div>
            </div>
          </div>
        </div>
 <!-- /.modal para mostrar los productos vendidos -->
        <div class="modal fade" id="modal-defaultt">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RECIBO</h4>
              </div>
              <div class="modal-body">
               



              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info btn-printt"> <span class="fa fa-print">Imprimir</span></button>
              </div>
            </div>
          </div>
        </div>
<div class="content-wrapper">
            <section class="content-header">
                <a href="<?php echo base_url();?>index.php/usuario/listaUsuario">Ir atras</a>
            </section>
            <section class="content-header">
                <h1 class="text-center">
                NUEVO USUARIO
                </h1>
                <br>
                <small>Ingrese los Siguentes Datos.</small>
            </section>

           
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">

                                <?php if($this->session->flashdata("error")):?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata("error");?></p>
                                </div>
                                <?php endif;?>

                                <?php echo form_open_multipart('usuarios/agregardb'); ?>
                                <div>

                                <form>
                                    <br>
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="nombre">Nombre: </label>
                                        <input type="text" name="nombre" placeholder="Ingrese nombre..."class="form-control" id="nombre" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfabeticos." >
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="primerApellido">Primer Apellido: </label>
                                        <input type="text" name="primerApellido" placeholder="Ingrese el primer apellido.."class="form-control" id="primerApellido" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfabeticos." >
                                    </div > 
                                    </div >                                

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="segundoApellido">Segundo Apellido: </label>
                                        <input type="text" name="segundoApellido" placeholder="Indrese el segundo apellido.."class="form-control" id="segundoApellido" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" title="Solo puede contener caracteres alfabeticos." >
                                    </div > 
                                    </div >
                                    <br>

                                    
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="ci">Carnet de Identidad: </label>
                                        <input type="text" name="ci" placeholder="Ingrese su número de identificacion C.I..."class="form-control" id="ci" pattern="[A-Za-z0-9-\s]+" required="" title="Solo puede contener caracteres alfa númericos." >
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="telefono">Telefono/Celular: </label>
                                        <input type="text" name="telefono" placeholder="Ingrese número de tel-cel..."class="form-control" id="telefono" pattern="[0-9\s]+" required="" title="Solo puede contener caracteres númericos" >
                                    </div > 
                                    </div >                                

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="roll">Categoria: </label>
                                        <select name="roll" class="form-control" id="roll" >
                                            <?php foreach($rol as $roll):?>
                                                <option value="<?php echo $roll->idRol;?>"><?php echo $roll->nombreRol; ?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div >
                                    </div >
                                    <br>
                               
                                    <div class="col-md-12">
                                    <div class=form-group>
                                        <label for="direccion">Dirección: </label>
                                        <input type="text" name="direccion" placeholder="Descriva su Direccion...."class="form-control" id="direccion" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfa númericos." >
                                    </div >
                                    </div >
                                    <div class="col-md-1">
                                    <div class=form-group>
                                        <label for="imagen">Imagen: </label>
                                        
                                        <input type="file" name="fotopost">
                                    </div >  
                                    </div >

                                     

                            
                                    
                                    <div class="col-md-12">
                                    <br>
                                    <br>
                                    <div>
                
                                        <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                        <?php echo form_close(); ?>

                                        <a href="<?=base_url()?>index.php/usuarios/listaUsuario" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div > 
                                    </div > 
                                    
                                </form>
                                </div > 
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       



<!--

<div class="content-wrapper">
            <section class="content-header">
                <h1>
                USUARIOS
                <small>Nuevo</small>
                </h1>
            </section>
            
            <!-- Main content -->
           <!-- <section class="content">
                <!-- Default box -->
               <!-- <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo form_open_multipart('Welcome/agregardbb'); ?>
                                   <div>
                                   
                                        

                                   <form action="<?php echo base_url();?>Welcome/agregardbb" method='POST'>
                                    <div class=form-group>
                                        <label for="ci">C.I.: </label>
                                        <input type="text" name="ci" placeholder="Ingrese el C.I. del usuario.."class="form-control" id="ci" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="primerApellido">Primer Apellido: </label>
                                        <input type="text" name="primerApellido" placeholder="Ingrese el 1er apellido del usuario.."class="form-control" id="primerApellido" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="segundoApellido">Segundo Apellido: </label>
                                        <input type="text" name="segundoApellido" placeholder="Ingrese el 2do apellido del usuario.."class="form-control" id="segundoApellido" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="nombre">Nombre: </label>
                                        <input type="text" name="nombre" placeholder="Ingrese el nombre del usuario.."class="form-control" id="nombre" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="telefono">Telefono: </label>
                                        <input type="text" name="telefono" placeholder="Ingrese el telefono del usuario.."class="form-control" id="telefono" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="direccion">Dirección: </label>
                                        <input type="text" name="direccion" placeholder="Ingrese la direccion del usuario.."class="form-control" id="direccion" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="rol">Rol: </label>
                                        <select name="rol" class="form-control" id="rol" >
                                            <?php foreach($roles as $rol):?>
                                                <option value="<?php echo $rol->idRoles;?>"><?php echo $rol->nombre;?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div >
                                    
                                    <div>
                                        <button type="submit" class="btn btn-primary">Registrar</button>
                                        <button type="submit" class="btn btn-primary">Cancelar</button>
                                    </div >


                                
                                        <div class="alert alert-danger" id="msg-error" style="text-align:left;">
                                            <strong >¡Importante!</strong>Corregir los siguentes errores.
                                            <?php echo validation_errors(); ?>
                                            <div class="list-errors"></div>
                                        </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>-->
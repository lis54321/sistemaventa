
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                  MODIFICANDO EL CLIENTE 
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                foreach ($razonSocial->result() as $row) {
                                ?>
                                <?php echo form_open_multipart('cliente/modificardb'); ?>
                            
                                <form action="/action_page.php">
                                    <div class=form-group>
                                    <input type="hidden" name="idCliente" value="<?php echo $row->idCliente; ?>">
                                     </div >
                                    <div class=form-group>
                                        <label for="razonSocial">Razon Social: </label>
                                        <input type="text" name="razonSocial" class="form-control" id="razonSocial" value="<?php echo $row->razonSocial; ?>"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >        
                                    <div class=form-group>
                                        <label for="nit"> NIT-CI: </label>
                                        <input type="text" name="nit" class="form-control" id="nit" value="<?php echo $row->nit; ?>" pattern="[0-9\s]+" title="Solo puede contener caracteres alfavetico.">
                                    </div >         
                                    <hr>                   
                                    <div class=form-group>
                                    
                                        <button type="submit" class="btn btn-oval btn-primary pull-center">Registrar</button>
                                        <?php echo form_close(); ?>
                                    
                                    <a href="<?=base_url()?>index.php/cliente/listaCliente" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div >  
                                          
                                    <?php
                                    }
                                    ?>
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       

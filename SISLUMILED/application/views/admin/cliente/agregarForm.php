        <div class="content-wrapper">
            <section class="content-header">
                <a href="<?php echo base_url();?>index.php/cliente/listaCliente">Ir atras</a>
            </section>
            <section class="content-header">
                <h1>
                REGISTRO DE UN NUEVO CLIENTE
                </h1>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">

                                <?php if($this->session->flashdata("error")):?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                                </div>
                                <?php endif;?>

                                 <?php echo form_open_multipart('cliente/agregardb'); ?>
                                
                                 <div>
                                      
                                    <form method="POST">
                                    
                                        <div class="form-group">
                                            <label for="razonSocial">Razon Social: </label>
                                            <input type="text" name="razonSocial" placeholder="Ingrese Razon Social.."class="form-control" id="razonSocial" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >
                                        <div class=form-group>
                                            <label for="nit">NIT-CI: </label>
                                            <input type="text" name="nit" placeholder="Descripción de la nit.."class="form-control" id="nit" pattern="[0-9\s]+" title="Solo puede contener caracteres numéricos." >
                                        </div >                
                                        <div>
                    
                                            <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                            <?php echo form_close(); ?>

                                            <a href="<?=base_url()?>index.php/cliente/listaCliente" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                        </div >  
                                        
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       







  
<footer id="footer">
    <!--<div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>BizPage</h3>
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">About us</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Services</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>-->

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>BizPage</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  
  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url();?>assets/templatss/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/superfish/hoverIntent.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/superfish/superfish.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/lightbox/js/lightbox.min.js"></script>
  <script src="<?php echo base_url();?>assets/templatss/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url();?>assets/templatss/contactform/contactform.js"></script>
  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url();?>assets/templatss/js/main.js"></script>








<!-- CORREGIR PARA VENTANA MODAL -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/template/dist/js/adminlte.min.js"></script>




<!-- DataTables -->
<script src="<?php echo base_url();?>assets/template/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables.net-bs/js/dataTables.bootstrap.js"></script>

<!-- AutoComplit -->
<script src="<?php echo base_url();?>assets/template/jquery-ui/jquery-ui.js"></script>
<!-- para imprimir de ventanas modales  -->
<script src="<?php echo base_url();?>assets/template/jquery-print/jquery.print.js"></script>






<script>
////// son para tablas 
$(document).ready(function () {
    var base_url="<?php echo base_url();?>";


/// modal para mostrar el detalle venta
    $(document).on("click",".btn-view-venta", function(){
       valorID = $(this).val(); 
       $.ajax({
           url: base_url + "venta/view",
           type:"POST",
           dataType:"html",
           data:{idVenta: valorID},
           success:function(data){
               $("#modal-default .modal-body").html(data);
           }
       });
    });
    $(document).on("click",".btn-view-venta", function(){
       valorID = $(this).val(); 
       $.ajax({
           url: base_url + "venta/recibo",
           type:"POST",
           dataType:"html",
           data:{idVenta: valorID},
           success:function(data){
               $("#modal-defaultt .modal-body").html(data);
           }
       });
    });



// pra imprimir de ventanas modales 
$(document).on("click",".btn-print",function(){
    $("#modal-default .modal-body").print();
})

// pra imprimir de ventanas modal de recibo 
$(document).on("click",".btn-printt",function(){
    $("#modal-defaultt .modal-body").print();
})

// para busqueda de las tablas
    $('#tabel').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron resultados en su busqueda",
                "searchPlaceholder": "Buscar registros",
                "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });
    $('.sidebar-menu').tree()

})

$('.sidebar-menu').tree();

// modal para mostrar a los clientes para venta
$(document).on("click",".btn-check",function(){
    cliente= $(this).val();
    infocliente=cliente.split("*");
    $("#idCliente").val(infocliente[0]);
    $("#cliente").val(infocliente[1]);
    $("#nit").val(infocliente[2]);
    $("#modal-defaul").modal("hide");
    
});

</script>






<script>
$(document).ready(function () {
    var base_url="<?php echo base_url();?>";

// para busqueda de las tablas
    $('#example1').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('.sidebar-menu').tree();
})



//modal para agregar productos a detalle venta

$(document).on("click",".btn-checkk",function(){
    producto = $(this).val();

    
    infoproducto  = producto.split("*");  
   
        idPrduct = Number($(this).closest("tr").find("td:eq(0)").text());

        html = "<tr>";
        html += "<td><input type='hidden' name='idProducto[]' value='"+infoproducto[0]+"'>"+infoproducto[1]+"</td>";
        html += "<td>"+infoproducto[2]+"</td>";
        html += "<td>"+infoproducto[3]+"</td>";
        html += "<td><input type='hidden' name='precio[]' value='"+infoproducto[4]+"'>"+infoproducto[4]+"</td>";
        html += "<td><input type='text' name='cantidad[]' value='1' class='cantidad'></td>";
        html += "<td><input type='hidden' name='importe[]' value='"+infoproducto[4]+"'><p>"+infoproducto[4]+"</p></td>";
        html += "<td><button type='button' class='btn btn-danger btn-remove-producto'><span class='fa fa-remove'></span></button></td>";
        html +="</tr>";
    
    if  (infoproducto == idPrduct)
    {
      alertify.error("Errrroororororo");
        
    }
        $("#tbventas tbody").append(html);
        sumar();
        $("#btn-checkk").val(null);
        $("#modal-defaull").modal("hide");
        //$("#producto").val(null);

    //}else{
    //alert("Ya inserto ese producto...");
    //}
});
// para eliminar product
$(document).on("click",".btn-remove-producto",function(){
    $(this).closest("tr").remove();
    sumar();
});
//para la cantidad
$(document).on("keyup","#tbventas input.cantidad", function(){
    cantidad = $(this).val();
    precio = $(this).closest("tr").find("td:eq(3)").text();
    importe = cantidad * precio;
    $(this).closest("tr").find("td:eq(5)").children("p").text(importe.toFixed(2));
     $(this).closest("tr").find("td:eq(5)").children("input").val(importe.toFixed(2));
    sumar();
 });
 function sumar(){
    total=0;
    $("#tbventas tbody tr").each(function(){
        total = total + Number($(this).find("td:eq(5)").text());

    });
    
    $("input[name=total]").val(total.toFixed(2));
}


</script>




</body>
</html>
